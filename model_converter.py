import torch
import argparse

from omegaconf import OmegaConf

from models import Generator


def convert_model(config_path, checkpoint_path, save_path):
    checkpoint = torch.load(checkpoint_path, map_location='cpu')

    hp = OmegaConf.load(config_path)

    model = Generator(hp)
    model.load_state_dict(checkpoint['model_g'])
    model.eval()

    model.remove_weight_norm()

    torch.save({
        'model_g': model.state_dict(),
    }, save_path)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='hifigan model converter')
    parser.add_argument('-c', '--config', required=True, type=str,
                        help='yaml file for configuration')
    parser.add_argument('-i', '--input_model_path', required=True, type=str,
                        help='training model path.')
    parser.add_argument('-o', '--output_model_path', required=True, type=str,
                        help='inference model path.')

    args = parser.parse_args()

    convert_model(args.config, args.input_model_path, args.output_model_path)
