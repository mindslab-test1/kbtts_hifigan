import os
import glob
import torch
import random
import numpy as np
from torch.utils.data import Dataset, DataLoader
from collections import Counter

from utils.utils import read_wav_np
from datasets.mel import mel_spectrogram


def create_dataloader(hp, args, train):
    if train:
        dataset = MelFromDisk(hp, hp.data.train_dir, hp.data.train_meta, args, train)
    else:
        dataset = MelFromDisk(hp, hp.data.val_dir, hp.data.val_meta, args, train)

    if train:
        return DataLoader(dataset=dataset, batch_size=hp.train.batch_size, shuffle=False,
            num_workers=hp.train.num_workers, pin_memory=True, drop_last=True)
    else:
        return DataLoader(dataset=dataset, batch_size=1, shuffle=False,
            num_workers=hp.train.num_workers, pin_memory=True, drop_last=False)


class MelFromDisk(Dataset):
    def __init__(self, hp, data_dir, metadata_path, args, train):
        self.hp = hp
        self.args = args
        self.train = train
        self.data_dir = data_dir
        metadata_path = os.path.join(data_dir, metadata_path)
        self.meta = self.load_metadata(metadata_path)

        self.mel_segment_length = hp.audio.segment_length // hp.audio.hop_length

        if train:
            # balanced sampling for each speaker
            speaker_counter = Counter((spk_id \
                                       for audiopath, text, spk_id in self.meta))
            weights = [1.0 / speaker_counter[spk_id] \
                       for audiopath, text, spk_id in self.meta]

            self.mapping_weights = torch.DoubleTensor(weights)


    def __len__(self):
        return len(self.meta)

    def __getitem__(self, idx):
        if self.train:
            idx1 = torch.multinomial(self.mapping_weights, 1).item()
            idx2 = torch.multinomial(self.mapping_weights, 1).item()
            return self.my_getitem(idx1), self.my_getitem(idx2)
        else:
            return self.my_getitem(idx)

    def my_getitem(self, idx):
        wavpath, _, _ = self.meta[idx]
        wavpath = os.path.join(self.data_dir, wavpath)
        sr, audio = read_wav_np(wavpath)

        if len(audio) < self.hp.audio.segment_length + self.hp.audio.pad_short and not self.args.finetune:
            audio = np.pad(audio, (0, self.hp.audio.segment_length + self.hp.audio.pad_short - len(audio)), \
                    mode='constant', constant_values=0.0)

        audio = torch.from_numpy(audio).unsqueeze(0)
        mel = self.get_mel(wavpath)

        if self.train:
            max_mel_start = mel.size(1) - self.mel_segment_length -1
            mel_start = random.randint(0, max_mel_start)
            mel_end = mel_start + self.mel_segment_length
            mel = mel[:, mel_start:mel_end]

            audio_start = mel_start * self.hp.audio.hop_length
            audio_len = self.hp.audio.segment_length
            audio = audio[:, audio_start:audio_start + audio_len]

        audio = audio + (1 / 32768) * torch.randn_like(audio)

        return mel, audio

    def get_mel(self, wavpath):
        if self.args.finetune: # Ground Truth Alignment Mel을 불러오는 부분
            melpath = '{}.predict'.format(wavpath)
            mel = torch.load(melpath, map_location='cpu')
            assert mel.size(0) == self.hp.audio.n_mel_channels, \
                'Mel dimension mismatch: expected %d, got %d' % \
                (self.hp.audio.n_mel_channels, mel.size(0))

        else: # Ground Truth Mel을 불러오는 부분
            melpath = wavpath.replace('.wav', '.mel')

            try:
                mel = torch.load(melpath, map_location='cpu')
                assert mel.size(0) == self.hp.audio.n_mel_channels, \
                    'Mel dimension mismatch: expected %d, got %d' % \
                    (self.hp.audio.n_mel_channels, mel.size(0))
            except (FileNotFoundError, RuntimeError, TypeError, AssertionError):
                sr, wav = read_wav_np(wavpath)
                assert sr == self.hp.audio.sampling_rate, \
                    'sample mismatch: expected %d, got %d at %s' % (self.hp.audio.sampling_rate, sr, wavpath)

                if len(wav) < self.hp.audio.segment_length + self.hp.audio.pad_short:
                    wav = np.pad(wav, (0, self.hp.audio.segment_length + self.hp.audio.pad_short - len(wav)), \
                                 mode='constant', constant_values=0.0)

                wav = torch.from_numpy(wav).unsqueeze(0)
                mel = mel_spectrogram(wav, self.hp.audio.filter_length, self.hp.audio.n_mel_channels,
                                      self.hp.audio.sampling_rate,
                                      self.hp.audio.hop_length,
                                      self.hp.audio.win_length,
                                      self.hp.audio.mel_fmin,
                                      self.hp.audio.mel_fmax, center=False)

                mel = mel.squeeze(0)

                torch.save(mel, melpath)

        return mel

    def load_metadata(self, path, split="|"):
        metadata = []
        with open(path, 'r', encoding='utf-8') as f:
            for line in f:
                stripped = line.strip().split(split)
                metadata.append(stripped)

        return metadata
