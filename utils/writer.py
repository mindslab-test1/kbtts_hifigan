from torch.utils.tensorboard import SummaryWriter

from .plotting import plot_waveform_to_numpy, plot_spectrogram_to_numpy
import numpy as np

class MyWriter(SummaryWriter):
    def __init__(self, hp, logdir):
        super(MyWriter, self).__init__(logdir)
        self.sample_rate = hp.audio.sampling_rate
        self.is_first = True

    def log_training(self, g_loss, d_loss, mel_loss, feat_loss, score_loss, step):
        self.add_scalar('0.train.g_loss', g_loss, step)
        self.add_scalar('1.train.d_loss', d_loss, step)
        
        self.add_scalar('2.train.score_loss', score_loss, step)
        self.add_scalar('3.train.feat_loss', feat_loss, step)
        self.add_scalar('4.train.mel_loss', mel_loss, step)

    def log_validation(self, generator, mpd, msd, val_loss, target, prediction, mel_fake, mel_real, step):

        self.add_audio('raw_audio_predicted_1', prediction[1], step, self.sample_rate)
        self.add_audio('raw_audio_predicted_2', prediction[2], step, self.sample_rate)
        self.add_audio('raw_audio_predicted', prediction[0], step, self.sample_rate)
        self.add_image('waveform_predicted', plot_waveform_to_numpy(prediction[2]), step)
        self.add_image('mel_predicted', plot_spectrogram_to_numpy(mel_fake), step)
        self.add_image('mel_error', plot_spectrogram_to_numpy(np.power(mel_real - mel_fake, 2)), step)
        self.add_scalar('5.val.mel_loss', val_loss, step)

        self.log_histogram(generator, step)
        self.log_histogram(mpd, step)
        self.log_histogram(msd, step)

        if self.is_first:
            self.add_audio('raw_audio_target_1', target[1], step, self.sample_rate)
            self.add_audio('raw_audio_target_2', target[2], step, self.sample_rate)
            self.add_audio('raw_audio_target', target[0], step, self.sample_rate)
            self.add_image('waveform_target', plot_waveform_to_numpy(target[2]), step)
            self.add_image('mel_target', plot_spectrogram_to_numpy(mel_real), step)
            self.is_first = False

    def log_histogram(self, model, step):
        for tag, value in model.named_parameters():
            self.add_histogram(tag.replace('.', '/'), value.cpu().detach().numpy(), step)
