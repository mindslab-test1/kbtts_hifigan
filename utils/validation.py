import tqdm
import torch
import torch.nn.functional as F
from datasets.mel import mel_spectrogram

def validate(hp, args, generator, mpd, msd, valloader, writer, step):
    generator.eval()
    torch.backends.cudnn.benchmark = False

    loader = tqdm.tqdm(valloader, desc='Validation loop')

    audio_list, fake_audio_list = [], []

    val_loss = 0

    for i,(mel, audio) in enumerate(loader):
        mel = mel.cuda()
        audio = audio.cuda()

        fake_audio = generator(mel)[:,:,:audio.size(2)]

        mel_fake = mel_spectrogram(fake_audio.squeeze(1), hp.audio.filter_length, hp.audio.n_mel_channels,
                                  hp.audio.sampling_rate,
                                  hp.audio.hop_length,
                                  hp.audio.win_length,
                                  hp.audio.mel_fmin,
                                  hp.audio.mel_fmax, center=False)
        mel_real = mel_spectrogram(audio.squeeze(1), hp.audio.filter_length, hp.audio.n_mel_channels,
                                  hp.audio.sampling_rate,
                                  hp.audio.hop_length,
                                  hp.audio.win_length,
                                  hp.audio.mel_fmin,
                                  hp.audio.mel_fmax, center=False)
        mel_loss = F.l1_loss(mel_real, mel_fake) * hp.loss.mel
        val_loss += mel_loss.item()

        if i in [0, len(loader)-1, len(loader)//2]:
            audio = audio[0][0].cpu().detach().numpy()
            fake_audio = fake_audio[0][0].cpu().detach().numpy()

            audio_list.append(audio)
            fake_audio_list.append(fake_audio)


    mel_fake = mel_fake[0].cpu().detach().numpy()
    mel_real = mel_real[0].cpu().detach().numpy()
    val_loss = val_loss / len(loader)

    writer.log_validation(generator, mpd, msd, val_loss, audio_list, fake_audio_list, mel_fake, mel_real, step)

    torch.backends.cudnn.benchmark = True
