import os
import threading
import math
import logging
import time
import torch
import grpc
import argparse
import signal

from concurrent import futures
from omegaconf import OmegaConf
from grpc_health.v1 import health
from grpc_health.v1 import health_pb2_grpc

from models import Generator

import tts_pb2
from tts_pb2_grpc import VocoderServicer
from tts_pb2_grpc import add_VocoderServicer_to_server

MAX_WAV_VALUE = 32768.0
_ONE_DAY_IN_SECONDS = 60 * 60 * 24


class HifiGanServicerImpl(VocoderServicer):
    def __init__(self, checkpoint_path, volume, mel_chunk_size,
                 crossfade_size, config_path, is_fp16, backup_path):
        super().__init__()
        self.default_config = {
            'config_path': config_path,
            'volume': str(volume)
        }

        self.lock = threading.Lock()
        self.running_set = set()
        self.waiting_queue = []

        self.backup_path = backup_path
        if backup_path != "":
            if os.path.exists(backup_path):
                logging.info('Load Config File: {}'.format(backup_path))
                backup_config = OmegaConf.load(backup_path)
                checkpoint_path = backup_config.path
                config_path = backup_config.config_path
                volume = backup_config.volume
            else:
                self._save_config(checkpoint_path, config_path, volume)
        self.model_status = tts_pb2.ModelStatus()

        self.is_fp16 = is_fp16
        self.mel_chunk_size = mel_chunk_size
        self.min_wav_value = -MAX_WAV_VALUE
        self.max_wav_value = MAX_WAV_VALUE - 1

        # cross-fade
        self.crossfade_size = crossfade_size
        self.left_filter = torch.linspace(start=1.0, end=0.0, steps=crossfade_size).to(0)
        self.right_filter = torch.linspace(start=0.0, end=1.0, steps=crossfade_size).to(0)

        try:
            self._load_checkpoint(checkpoint_path, config_path, volume)
        except Exception as e:
            self.model_status.state = tts_pb2.MODEL_STATE_NONE
            self.model_status.model.Clear()
            logging.exception(e)

    @torch.no_grad()
    def Mel2Wav(self, in_mel, context):
        try:
            result = self.SetModel(in_mel.model, context, True)
            if not result.result:
                context.set_code(grpc.StatusCode.INVALID_ARGUMENT)
                context.set_details(result.error)
                return tts_pb2.WavData()
            if self.model_status.state != tts_pb2.MODEL_STATE_RUNNING:
                context.set_code(grpc.StatusCode.UNAVAILABLE)
                context.set_details(tts_pb2.ModelState.Name(self.model_status.state))
                return tts_pb2.WavData()

            mel_total = torch.FloatTensor([d for d in in_mel.data])
            mel_total = mel_total.view(1, self.hp.audio.n_mel_channels, -1)

            previous_audio = None
            for start_idx in range(0, mel_total.size(-1), self.mel_chunk_size):
                mel = mel_total[:, :, start_idx:start_idx+self.mel_chunk_size]
                audio, previous_audio = self._mel2wav(mel, previous_audio)
                yield tts_pb2.WavData(data=audio)

        except Exception as e:
            logging.exception(e)
            context.set_code(grpc.StatusCode.UNKNOWN)
            context.set_details(str(e))

        finally:
            with self.lock:
                self.running_set.discard(threading.get_ident())

    @torch.no_grad()
    def StreamMel2Wav(self, in_mel_iterator, context):
        try:
            previous_audio = None
            is_first = True
            for in_mel in in_mel_iterator:
                if is_first:
                    result = self.SetModel(in_mel.model, context, True)
                    if not result.result:
                        context.set_code(grpc.StatusCode.INVALID_ARGUMENT)
                        context.set_details(result.error)
                        return tts_pb2.WavData()
                    if self.model_status.state != tts_pb2.MODEL_STATE_RUNNING:
                        context.set_code(grpc.StatusCode.UNAVAILABLE)
                        context.set_details(tts_pb2.ModelState.Name(self.model_status.state))
                        return tts_pb2.WavData()
                    is_first = False

                mel = torch.FloatTensor([d for d in in_mel.data])
                mel = mel.view(1, self.hp.audio.n_mel_channels, -1)

                audio, previous_audio = self._mel2wav(mel, previous_audio)
                yield tts_pb2.WavData(data=audio)

        except Exception as e:
            logging.exception(e)
            context.set_code(grpc.StatusCode.UNKNOWN)
            context.set_details(str(e))

        finally:
            with self.lock:
                self.running_set.discard(threading.get_ident())

    def GetModel(self, empty, context):
        try:
            return self.model_status
        except Exception as e:
            logging.exception(e)
            context.set_code(grpc.StatusCode.UNKNOWN)
            context.set_details(str(e))

    def SetModel(self, in_model, context, is_running=False):
        try:
            del_keys = [
                config_name
                for config_name in in_model.config.keys()
                if config_name not in self.default_config.keys()
            ]
            for del_key in del_keys:
                del in_model.config[del_key]
            for config_name, config_value in self.default_config.items():
                if config_name not in in_model.config.keys() or in_model.config[config_name] == '':
                    in_model.config[config_name] = config_value
            result = None
            waiting_key = None
            with self.lock:
                if 0 < len(self.waiting_queue):
                    waiting_key = threading.get_ident()
                    self.waiting_queue.append(waiting_key)
                else:
                    if in_model is None or in_model.path == "" or in_model == self.model_status.model:
                        if is_running:
                            self.running_set.update([threading.get_ident()])
                        result = tts_pb2.SetModelResult(result=True)
                    elif in_model != self.model_status.model:
                        if 0 < len(self.running_set):
                            waiting_key = threading.get_ident()
                            self.waiting_queue.append(waiting_key)
                        else:
                            if is_running:
                                self.running_set.update([threading.get_ident()])
                            result = self._set_model(in_model)

            if waiting_key is not None:
                while True:
                    with self.lock:
                        if self.waiting_queue[0] == waiting_key:
                            if in_model == self.model_status.model or in_model.path == "":
                                if is_running:
                                    self.running_set.update([threading.get_ident()])
                                del self.waiting_queue[0]
                                result = tts_pb2.SetModelResult(result=True)
                                break
                            elif in_model != self.model_status.model:
                                if 0 == len(self.running_set):
                                    if is_running:
                                        self.running_set.update([threading.get_ident()])
                                    del self.waiting_queue[0]
                                    result = self._set_model(in_model)
                                    break
                    time.sleep(0.01)
            return result
        except Exception as e:
            logging.exception(e)
            context.set_code(grpc.StatusCode.UNKNOWN)
            context.set_details(str(e))

    def _mel2wav(self, mel, previous_audio):
        mel = mel.to(0)
        if mel.size(-1) < 4:
            # minimum value of mel from datasets/mel.py is -5.0.
            # minimal time frame for generator is 3 - size of nn.ReflectionPad1d(3).
            pad_size = 4 - mel.size(-1)
            mel = torch.nn.functional.pad(mel, (0, pad_size), 'constant', value=-5.0)

        audio = self.model(mel)
        audio = MAX_WAV_VALUE * audio.squeeze() * self.volume

        # cross-fade. this shortens the audio about crossfade_size per chunk.
        if previous_audio is not None:
            audio[:self.crossfade_size] = \
                previous_audio * self.left_filter + \
                audio[:self.crossfade_size] * self.right_filter
        previous_audio = audio[-self.crossfade_size:]
        audio = audio[:-self.crossfade_size]

        audio = torch.clamp(audio, min=self.min_wav_value, max=self.max_wav_value)
        audio = audio.short().tolist()
        return audio, previous_audio

    def GetMelConfig(self, empty, context):
        mel_config = tts_pb2.MelConfig(
            filter_length=self.hp.audio.filter_length,
            hop_length=self.hp.audio.hop_length,
            win_length=self.hp.audio.win_length,
            n_mel_channels=self.hp.audio.n_mel_channels,
            sampling_rate=self.hp.audio.sampling_rate,
            mel_fmin=self.hp.audio.mel_fmin,
            mel_fmax=self.hp.audio.mel_fmax
        )
        return mel_config

    def _set_model(self, in_model):
        logging.debug('_set_model/in_model/%s', in_model)
        try:
            if not os.path.exists(in_model.path):
                raise RuntimeError("Model {} does not exist.".format(in_model.path))
            config_path = in_model.config["config_path"]
            if config_path == "":
                config_path = self.model_status.model.config["config_path"]
            volume = float(in_model.config["volume"])
        except Exception as e:
            logging.exception(e)
            return tts_pb2.SetModelResult(result=False, error=str(e))

        try:
            self._load_checkpoint(in_model.path, config_path, volume)
            self._save_config(in_model.path, config_path, volume)
            return tts_pb2.SetModelResult(result=True)
        except Exception as e:
            logging.exception(e)
            self.model_status.state = tts_pb2.MODEL_STATE_NONE
            self.model_status.model.Clear()
            return tts_pb2.SetModelResult(result=False, error=str(e))

    def _load_checkpoint(self, model_filename, config_path, volume):
        self.model_status.state = tts_pb2.MODEL_STATE_LOADING

        self.volume = math.pow(10, 0.5 * math.log2(volume))
        if model_filename != self.model_status.model.path:
            if config_path != self.model_status.model.config["config_path"]:
                hp = OmegaConf.load(config_path)
                self.hp = hp

                self.model = Generator(hp).to(0)
                self.model.eval()

                self.model.remove_weight_norm()
                if self.is_fp16:
                    from apex import amp
                    self.model, _ = amp.initialize(self.model, [], opt_level="O3")
                torch.cuda.empty_cache()
                self.model_status.model.config["config_path"] = config_path

            checkpoint = torch.load(model_filename, map_location='cuda:0')
            self.model.load_state_dict(checkpoint['model_g'])

            self.model_status.model.path = model_filename

        self.model_status.model.config["volume"] = str(volume)
        self.model_status.state = tts_pb2.MODEL_STATE_RUNNING

    def _save_config(self, model_filename, config_path, volume):
        if self.backup_path != "":
            config = OmegaConf.create({
                'path': model_filename,
                'config_path': config_path,
                'volume': volume
            })
            OmegaConf.save(config, self.backup_path)
            logging.debug('Save Config File: {}'.format(self.backup_path))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='hifigan runner executor')
    parser.add_argument('-c', '--config', required=True, type=str,
                        help='yaml file for configuration')
    parser.add_argument('-m', '--model', required=True, type=str,
                        help='Model Path.')
    parser.add_argument('-l', '--log_level', default='INFO', type=str,
                        help='logger level')
    parser.add_argument('-p', '--port', default=35003, type=int,
                        help='grpc port')
    parser.add_argument('-v', '--volume', default=1.0, type=float,
                        help='audio volume')
    parser.add_argument('-w', '--max_workers', default=32, type=int,
                        help='max workers')
    parser.add_argument('--crossfade_size', default=200, type=int,
                        help='window size for cross-fade')
    parser.add_argument('--mel_chunk_size', default=88, type=int,
                        help='mel chunk size for Mel2Wav rpc')
    parser.add_argument('--is_fp16', action='store_true',
                        help='fp16 mode')
    parser.add_argument('-b', '--backup_config', type=str, default="",
                        help='yaml file for backup configuration')

    args = parser.parse_args()

    logging.basicConfig(
        level=getattr(logging, args.log_level),
        format='[%(levelname)s|%(filename)s:%(lineno)s][%(asctime)s] >>> %(message)s'
    )

    hifigan = HifiGanServicerImpl(
        args.model, args.volume, args.mel_chunk_size,
        args.crossfade_size, args.config, args.is_fp16, args.backup_config)

    server = grpc.server(futures.ThreadPoolExecutor(max_workers=args.max_workers), )
    add_VocoderServicer_to_server(hifigan, server)
    server.add_insecure_port('[::]:{}'.format(args.port))
    health_servicer = health.HealthServicer()
    health_pb2_grpc.add_HealthServicer_to_server(health_servicer, server)
    server.start()

    def exit_gracefully(signum, frame):
        health_servicer.enter_graceful_shutdown()
        server.stop(60)
    signal.signal(signal.SIGINT, exit_gracefully)
    signal.signal(signal.SIGTERM, exit_gracefully)
    logging.info('hifigan starting at 0.0.0.0:%d', args.port)

    server.wait_for_termination()
